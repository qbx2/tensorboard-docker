FROM python:3.9.1
RUN apt-get update && \
    apt-get install -y libjemalloc-dev && \
    rm -rf /var/lib/apt/lists/* && \
    [ -f /usr/lib/x86_64-linux-gnu/libjemalloc.so ]
ENV LD_PRELOAD /usr/lib/x86_64-linux-gnu/libjemalloc.so
RUN pip install tensorboard==2.10.1
CMD ["tensorboard"]
